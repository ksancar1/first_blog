<?php

namespace Database\Factories;

use App\Models\Rubric;
use Illuminate\Database\Eloquent\Factories\Factory;

class RubricFactory extends Factory
{

    protected $model = Rubric::class;

    public function definition(): array
    {
        return [
            "name"       => $this->faker->unique()->words(3, true),
            "created_at" => $this->faker->dateTimeThisYear,
        ];
    }
}
