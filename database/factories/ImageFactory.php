<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class ImageFactory extends Factory
{
    protected $model = Image::class;

    public function definition(): array
    {
        return [
            "filename" => Str::lower($this->faker->words(2, true) . ".jpg"),
            "is_cover" => $this->faker->boolean
        ];
    }
}
