<?php

namespace Database\Factories;

use App\Models\Article;
use Illuminate\Database\Eloquent\Factories\Factory;


class ArticleFactory extends Factory
{
    protected $model = Article::class;

    public function definition(): array
    {
        return [
            "name"        => $this->faker->unique()->words(3, true),
            "description" => $this->faker->words(20, true),
            "content"     => $this->faker->words(70, true),
            "created_at"  => $this->faker->dateTimeThisYear,
            "user_id"     => $this->faker->randomElement([
                "00000000-0000-0000-0000-000000000001",
                "00000000-0000-0000-0000-000000000002",
                "00000000-0000-0000-0000-000000000003",
                "00000000-0000-0000-0000-000000000004",
                "00000000-0000-0000-0000-000000000005",
                "00000000-0000-0000-0000-000000000006",
                "00000000-0000-0000-0000-000000000008",
                "00000000-0000-0000-0000-000000000009",
                "00000000-0000-0000-0000-000000000010",
            ])
        ];
    }
}
