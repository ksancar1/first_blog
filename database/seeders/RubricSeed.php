<?php

namespace Database\Seeders;

use App\Models\Article;
use App\Models\Comment;
use App\Models\Rubric;
use Illuminate\Database\Seeder;

class RubricSeed extends Seeder
{
    public function run(): void
    {
        Rubric::factory()
            ->count(20)
            ->has(Article::factory()->count(rand(1, 15))->has(Comment::factory()->count(7)))
            ->create();
    }
}
