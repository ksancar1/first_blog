<?php

namespace Database\Seeders;

use App\Enums\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $password = bcrypt("meganote");
        collect([
            ["id" => "00000000-0000-0000-0000-000000000006", "name" => "Editor", "email" => "editor@com.ua", "role" => Role::EDITOR],
            ["id" => "00000000-0000-0000-0000-000000000002", "name" => "Editor1", "email" => "editor1@com.ua", "role" => Role::EDITOR],
            ["id" => "00000000-0000-0000-0000-000000000003", "name" => "Editor2", "email" => "editor2@com.ua", "role" => Role::EDITOR],
            ["id" => "00000000-0000-0000-0000-000000000004", "name" => "Editor3", "email" => "editor3@com.ua", "role" => Role::EDITOR],
            ["id" => "00000000-0000-0000-0000-000000000005", "name" => "Editor4", "email" => "editor4@com.ua", "role" => Role::EDITOR],
            ["id" => "00000000-0000-0000-0000-000000000001", "name" => "Admin", "email" => "admin@com.ua", "role" => Role::ADMIN],
            ["id" => "00000000-0000-0000-0000-000000000007", "name" => "Kubik", "email" => "kubik@com.ua", "role" => Role::KUBIK],
            ["id" => "00000000-0000-0000-0000-000000000008", "name" => "VIP", "email" => "vip@com.ua", "role" => Role::VIP],
            ["id" => "00000000-0000-0000-0000-000000000009", "name" => "Subscriber", "email" => "subscriber@com.ua", "role" => Role::SUBSCRIBER],
            ["id" => "00000000-0000-0000-0000-000000000010", "name" => "Guest", "email" => "guest@com.ua", "role" => Role::GUEST],
        ])
            ->map(function (array $data) use ($password) {
                $user = new User();
                $user->id = $data["id"];
                $user->name = $data["name"];
                $user->email    = $data["email"];
                $user->password = $password;
                $user->role     = $data["role"];
                $user->save();
            });

    }
}
