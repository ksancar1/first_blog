<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create("articles", function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table
                ->foreignUuid("rubric_id")
                ->nullable()
                ->constrained("rubrics")
                ->cascadeOnDelete();
            $table
                ->foreignuuid("user_id")
                ->constrained("users")
                ->cascadeOnDelete();
            $table->string("name")->nullable();
            $table->mediumText("description")->nullable();
            $table->longText("content")->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists("articles");
    }
};
