<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create("comments", function (Blueprint $table) {
            $table->uuid("id")->primary();
            $table
                ->foreignUuid("article_id")
                ->nullable()
                ->constrained("articles")
                ->cascadeOnDelete();
            $table->string("name")->nullable();
            $table->string("email")->nullable();
            $table->mediumText("description")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists("comments");
    }
};
