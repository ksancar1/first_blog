<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @property string id
 * @property string name
 * @property string description
 * @property string content
 * @property string rubric_id
 * @property Collection comments
 * @property Collection images
 * @property Image cover
 */
class Article extends Model
{
    use HasFactory, HasUuids, SoftDeletes;

    public function rubric(): BelongsTo
    {
        return $this->belongsTo(Rubric::class);
    }

    public function comments(): HasMany
    {
        return $this->hasMany(Comment::class)->latest();
    }

    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, "user_id");
    }

    public function images(): MorphMany
    {
        return $this->morphMany(Image::class, "content");
    }

    public function cover(): MorphOne
    {
        return $this->morphOne(Image::class, "content")->where("is_cover", true);
    }

    public function url(): string
    {
        return route("article", $this);
    }

    public function hasImages(): bool
    {
        return $this->images()->count() > 0;
    }
}
