<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Collection;

/**
 * @property string id
 * @property string name
 * @property Collection articles
 */
class Rubric extends Model
{
    use HasFactory, HasUuids, SoftDeletes;

    public function articles(): HasMany
    {
        return $this->hasMany(Article::class);
    }

    public function cover(): MorphOne
    {
        return $this->morphOne(Image::class, "content");
    }

    public function url(): string
    {
        return route("rubric", $this);
    }
}
