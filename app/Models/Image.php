<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Facades\Storage;

/**
 * @property string id
 * @property string filename
 * @property string content_id
 * @property string content_type
 * @property bool is_cover
 */
class Image extends Model
{
    use HasFactory, HasUuids;

    public $timestamps = false;
    protected $fillable = ["is_cover"];

    public function content(): MorphTo
    {
        return $this->morphTo();
    }

    public function getUrl(): string
    {
        return Storage::disk("images")->url($this->content_id . "/" . $this->filename);
    }
}
