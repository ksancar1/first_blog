<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property string description
 * @property string name
 * @property string email
 * @property string article_id
 */
class Comment extends Model
{
    use HasFactory, HasUuids;

    public function article(): BelongsTo
    {
        return $this->belongsTo(Article::class);
    }
}
