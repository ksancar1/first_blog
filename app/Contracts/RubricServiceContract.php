<?php

namespace App\Contracts;

use App\Http\Requests\SearchRequest;
use Illuminate\Database\Eloquent\Collection;

interface RubricServiceContract
{
    public function search(SearchRequest $request): Collection;

    public function getFilled(): Collection;
}
