<?php

namespace App\Contracts;

use App\DTO\CommentDTO;

interface CommentServiceContract
{
    public function store(CommentDTO $dto): void;
}
