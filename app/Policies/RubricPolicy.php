<?php

namespace App\Policies;

use App\Enums\Role;
use App\Models\Rubric;
use App\Models\User;


class RubricPolicy
{

    public function viewAny(User $user): bool
    {
        return true;
    }


    public function view(User $user, Rubric $rubric): bool
    {
        return $user->id === $rubric->user_id;
    }


    public function create(User $user): bool
    {
        return $user->role !== Role::KUBIK;
    }


    public function update(User $user, Rubric $rubric): bool
    {
        return $user->role === Role::ADMIN;
    }


    public function delete(User $user, Rubric $rubric): bool
    {
        return $user->role === Role::ADMIN;
    }


    public function restore(User $user): bool
    {
        return $user->role === Role::KUBIK || $user->role === Role::ADMIN;
    }


    public function forceDelete(User $user, Rubric $rubric): bool
    {
        //
    }
}
