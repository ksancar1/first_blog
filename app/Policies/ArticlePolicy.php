<?php

namespace App\Policies;

use App\Enums\Role;
use App\Models\Article;
use App\Models\User;

class ArticlePolicy
{

    public function viewAny(User $user): bool
    {
        return true;
    }

    public function view(User $user, Article $article): bool
    {
        return $user->id === $article->user_id;
    }

    public function create(User $user): bool
    {
        return $user->role !== Role::KUBIK;
    }

    public function update(User $user, Article $article): bool
    {
        return ($user->id === $article->user_id &&
                $user->role === Role::EDITOR) || $user->role === Role::ADMIN;
    }

    public function delete(User $user, Article $article): bool
    {
        return $user->id === $article->user_id &&
            $user->role === Role::EDITOR || $user->role === Role::ADMIN;
    }

    public function restore(User $user): bool
    {
        return $user->role === Role::KUBIK || $user->role === Role::ADMIN;
    }

    public function forceDelete(User $user, Article $article): bool
    {
        //
    }
}
