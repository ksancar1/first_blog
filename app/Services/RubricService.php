<?php

namespace App\Services;

use App\Contracts\RubricServiceContract;
use App\Http\Requests\SearchRequest;
use App\Models\Rubric;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Database\Eloquent\Collection;

class RubricService implements RubricServiceContract
{
    public function search(SearchRequest $request): Collection
    {
        return Rubric::query()
            ->whereHas("articles", fn(Builder $builder) => $builder
                ->where("name", "ilike", "%" . $request->validated("query") . "%")
                ->select("id")
            )
            ->withCount([
                "articles" => fn(Builder $builder) => $builder
                    ->where("name", "ilike", "%" . $request->validated("query") . "%")
            ])
            ->get();
    }

    public function getFilled(): Collection
    {
        return Rubric::query()->withCount("articles")->get();
    }
}
