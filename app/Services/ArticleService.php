<?php

namespace App\Services;

use App\Http\Requests\SearchRequest;
use App\Models\Article;
use Illuminate\Contracts\Database\Query\Builder;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ArticleService
{
    public function search(SearchRequest $request): LengthAwarePaginator
    {
        return Article::query()
            ->where("name", "ilike", "%" . $request->validated("query") . "%")
            ->when(!is_null($request->validated("rubric_id")), fn(Builder $builder) => $builder
                ->where("rubric_id", "=", $request->validated("rubric_id")))
            ->latest()
            ->paginate(10);
    }

    public function lastArticles(int $count = 10): LengthAwarePaginator
    {
        return Article::query()->with(["rubric", "cover"])->latest()->paginate($count);
    }
}
