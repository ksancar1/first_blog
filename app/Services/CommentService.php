<?php

namespace App\Services;

use App\Contracts\CommentServiceContract;
use App\DTO\CommentDTO;
use App\Models\Comment;

class CommentService implements CommentServiceContract
{
    public function store(CommentDTO $dto): void
    {
        $comment              = new Comment;
        $comment->name        = $dto->getName();
        $comment->email       = $dto->getEmail();
        $comment->description = $dto->getDescription();
        $comment->article_id  = $dto->getArticleId();
        $comment->save();
    }
}
