<?php

namespace App\DTO;

use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;

class CommentDTO implements Jsonable, Arrayable
{
    public function __construct(readonly private array $data)
    {
    }

    public function getName(): string
    {
        return $this->data["name"] ?? "";
    }

    public function getEmail(): string
    {
        return $this->data["email"] ?? "";
    }

    public function getDescription(): string
    {
        return $this->data["description"] ?? "";
    }

    public function getArticleId(): string
    {
        return $this->data["article_id"] ?? "";
    }

    public function toArray(): array
    {
        return $this->data;
    }

    public function toJson($options = 0): string
    {
        return json_encode($this->data, $options);
    }
}
