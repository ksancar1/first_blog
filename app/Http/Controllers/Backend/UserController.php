<?php

namespace App\Http\Controllers\Backend;

use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\User\CreateRequest;
use App\Http\Requests\Backend\User\UpdateRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class UserController extends Controller
{

    public function index(): View
    {
        $users = User::query()
            ->where("name", "ilike", "%" . request("q", "") . "%")
            ->latest()
            ->paginate(10)
            ->appends(["q" => request("q")]);

        return view("backend.user.index", compact("users"));
    }

    public function create(): View
    {
        return view("backend.user.create", [
            "roles" => Role::cases()
        ]);
    }

    public function store(CreateRequest $request): RedirectResponse
    {
        $user           = new User();
        $user->name     = $request->validated("name");
        $user->email    = $request->validated("email");
        $user->role     = $request->validated("role");
        $user->password = bcrypt($request->validated("password"));
        $user->save();

        return redirect()
            ->route("admin.user.index")
            ->with("success", "User has been created successfully");
    }

    public function edit(User $user): View
    {
        return view("backend.user.edit", [
            "user"  => $user,
            "roles" => Role::cases()
        ]);
    }

    public function update(UpdateRequest $request, string $user): RedirectResponse
    {
        $user        = User::find($user);
        $user->name  = $request->validated("name");
        $user->email = $request->validated("email");
        $user->role  = $request->validated("role");
        if ($request->filled("password")) {
            $user->password = bcrypt($request->validated("password"));
        }
        $user->save();

        return redirect()
            ->route("admin.user.index")
            ->with("success", "User has been updated successfully");
    }

    public function destroy(User $user): RedirectResponse
    {
        $user->delete();

        return redirect()
            ->route("admin.user.index")
            ->with("success", "User has been deleted successfully");
    }
}
