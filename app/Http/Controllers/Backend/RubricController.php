<?php

namespace App\Http\Controllers\Backend;

use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Rubric\CreateRequest;
use App\Http\Requests\Backend\Rubric\DeleteRequest;
use App\Http\Requests\Backend\Rubric\RestoreRequest;
use App\Http\Requests\Backend\Rubric\UpdateRequest;
use App\Models\Rubric;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;

class RubricController extends Controller
{
    public function index(): View
    {
        $user = auth()->user();

        $rubrics = Rubric::query()
            ->when($user->role === Role::KUBIK, function (Builder $builder) {
                return $builder->onlyTrashed();
            })
            ->where("name", "ilike", "%" . request("q", "") . "%")
            ->withCount("articles")
            ->latest()
            ->paginate(10)
            ->appends(["q" => request("q")]);

        return view("backend.rubric.index", compact("rubrics", "user"));
    }

    public function create(): View
    {
        return view("backend.rubric.create");
    }

    public function store(CreateRequest $request): RedirectResponse
    {
        $rubric       = new Rubric();
        $rubric->name = $request->validated("name");
        $rubric->save();

        return redirect()
            ->route("admin.rubric.index")
            ->with("success", "Rubric has been created successfully");
    }

    public function edit(Rubric $rubric): View
    {
        return view("backend.rubric.edit", compact("rubric"));
    }

    public function update(UpdateRequest $request, string $rubric): RedirectResponse
    {
        $rubric       = Rubric::find($rubric);
        $rubric->name = $request->validated("name");
        $rubric->save();

        return redirect()
            ->route("admin.rubric.edit", $rubric)
            ->with("success", "Rubric has been updated successfully");
    }

    public function destroy(DeleteRequest $request): RedirectResponse
    {
        $rubric = Rubric::find($request->validated("id"));
        $rubric->articles()->delete();
        $rubric->delete();

        return redirect()
            ->route("admin.rubric.index")
            ->with("success", "Rubric has been deleted successfully");
    }

    public function restore(RestoreRequest $request): RedirectResponse
    {
        Rubric::withTrashed()->find($request->validated("id"))->restore();

        return redirect()
            ->route("admin.rubric.index")
            ->with("success", "Rubric has been restored successfully");
    }
}
