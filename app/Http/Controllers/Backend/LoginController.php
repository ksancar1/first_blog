<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\LoginRequest;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class LoginController extends Controller
{

    public function form(): View
    {
        return view("backend.login.login");
    }

    public function login(LoginRequest $request): RedirectResponse
    {
        /** @var User $user */
        $user = User::query()->where("email", $request->validated("email"))->first();
        if (is_null($user) || !Hash::check($request->validated("password"), $user->password)) {
            return redirect()
                ->route("admin.form")
                ->withInput(["email" => $request->validated("email")])
                ->with("error", "Invalid credentials");
        }
        Auth::login($user);

        return redirect()->route("admin.dashboard");
    }

    public function logout(): RedirectResponse
    {
        Auth::logout();

        return redirect()->route("admin.form");
    }
}
