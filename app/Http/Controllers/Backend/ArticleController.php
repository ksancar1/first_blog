<?php

namespace App\Http\Controllers\Backend;

use App\Enums\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\Backend\Article\CreateRequest;
use App\Http\Requests\Backend\Article\DeleteRequest;
use App\Http\Requests\Backend\Article\RestoreRequest;
use App\Http\Requests\Backend\Article\UpdateRequest;
use App\Models\Article;
use App\Models\Image;
use App\Models\Rubric;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ArticleController extends Controller
{

    public function index(): View
    {
        $user = auth()->user();

        $articles = Article::query()
            ->when($user->role === Role::EDITOR, function (Builder $builder) use ($user) {
                return $builder->where("user_id", "=", $user->id);
            })
            ->when($user->role === Role::KUBIK, function (Builder $builder) {
                return $builder->onlyTrashed();
            })
            ->where(fn(Builder $builder) => $builder
                ->where("name", "ilike", "%" . request("q", "") . "%")
                ->orWhere("description", "ilike", "%" . request("q", "") . "%")
                ->orWhere("content", "ilike", "%" . request("q", "") . "%"))
            ->with(["rubric:id,name", "author:id,name"])
            ->withCount("comments")
            ->latest()
            ->paginate(20)
            ->appends(["q" => request("q")]);

        return view("backend.article.index", compact("articles", "user"));
    }


    public function create(): View
    {
        $rubrics = Rubric::query()->orderBy("name")->get();

        return view("backend.article.create", compact("rubrics"));
    }

    public function store(CreateRequest $request): RedirectResponse
    {
        $article              = new Article();
        $article->user_id     = auth()->id();
        $article->name        = $request->validated("name");
        $article->rubric_id   = $request->validated("rubric_id");
        $article->description = $request->validated("description");
        $article->content     = $request->validated("content");
        $article->save();

        $this->uploadImages($article, $request->file("images"));

        return redirect()
            ->route("admin.article.index")
            ->with("success", "Article has been created successfully");
    }


    public function edit(Article $article): View
    {
        $rubrics = Rubric::query()->orderBy("name")->get();
        $article->load("images");

        return view("backend.article.edit", compact("article", "rubrics"));
    }


    public function update(UpdateRequest $request, string $article): RedirectResponse
    {
        $article              = Article::find($article);
        $article->name        = $request->validated("name");
        $article->rubric_id   = $request->validated("rubric_id");
        $article->description = $request->validated("description");
        $article->content     = $request->validated("content");
        $article->save();

        $this->uploadImages($article, $request->file("images"));

        if (!is_null($request->get("cover"))) {
            $article->images()->update(["is_cover" => false]);
            $article->images()->find($request->get("cover"))->update(["is_cover" => true]);
        }

        return redirect()
            ->route("admin.article.edit", $article)
            ->with("success", "Article has been updated successfully");
    }


    public function destroy(DeleteRequest $request): RedirectResponse
    {
        Article::find($request->validated("id"))->delete();

        return redirect()
            ->route("admin.article.index")
            ->with("success", "Article has been deleted successfully");
    }


    public function restore(RestoreRequest $request): RedirectResponse
    {
        Article::withTrashed()->find($request->validated("id"))->restore();

        return redirect()
            ->route("admin.article.index")
            ->with("success", "Article has been restored successfully");
    }

    private function uploadImages(Article $article, ?array $files): void
    {
        if (is_null($files)) {
            return;
        }
        $storage = Storage::disk("images");

        $storage->makeDirectory($article->id);

        collect($files)->each(function (?UploadedFile $file) use ($storage, $article) {
            $fileName = Str::of(Str::random(24))->lower()->toString() . "." . $file->clientExtension();
            $storage->put($article->id . "/" . $fileName, $file->getContent());

            $image           = new Image();
            $image->filename = $fileName;

            $article->images()->save($image);
        });
    }
}
