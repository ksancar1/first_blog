<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function __invoke(Request $request): View
    {
        $users = User::query()
            ->withCount([
                "articles",
                "articles as articles_deleted_count" => function (Builder $builder) {
                    return $builder->onlyTrashed();
                }
            ])
            ->orderByDesc("articles_count")
            ->take(10)
            ->get();
        return view("backend.dashboard", ["user" => $request->user(), "users" => $users]);
    }
}
