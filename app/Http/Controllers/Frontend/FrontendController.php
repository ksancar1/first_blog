<?php

namespace App\Http\Controllers\Frontend;

use App\Contracts\CommentServiceContract;
use App\Contracts\RubricServiceContract;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateCommentRequest;
use App\Http\Requests\SearchRequest;
use App\Models\Article;
use App\Models\Rubric;
use App\Services\ArticleService;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class FrontendController extends Controller
{
    public function __construct(
        readonly private ArticleService         $articleService,
        readonly private CommentServiceContract $commentService,
        readonly private RubricServiceContract  $rubricService)
    {
    }

    public function index(): View
    {
        return view("frontend.frontpage.index", [
            "rubrics"  => $this->rubricService->getFilled(),
            "articles" => $this->articleService->lastArticles()
        ]);
    }

    public function rubric(Rubric $rubric): View
    {
        return view("frontend.rubric.index", [
            "rubric"   => $rubric,
            "articles" => $rubric->articles()->paginate(10)
        ]);
    }

    public function article(Article $article): View
    {
        $article->load("comments");

        return view("frontend.article.index", compact("article"));
    }

    public function search(SearchRequest $request): View
    {
        return view("frontend.search.index", [
            "articles" => $this->articleService->search($request),
            "rubrics"  => $this->rubricService->search($request)
        ]);
    }

    public function storeComment(CreateCommentRequest $request): RedirectResponse
    {
        $this->commentService->store($request->dto());

        return redirect()
            ->route("article", ["article" => $request->dto()->getArticleId()])
            ->with("success", "Comment has been created successfully");
    }
}
