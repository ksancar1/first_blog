<?php

namespace App\Http\Requests;

use App\DTO\CommentDTO;
use App\Models\Article;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateCommentRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "name"        => ["required", "string", "min:3", "max:30"],
            "email"       => ["required", "string", "email"],
            "description" => ["required", "string", "min:3", "max:300"],
            "article_id"  => ["required", "bail", "uuid", Rule::exists((new Article())->getTable(), "id")]
        ];
    }

    public function dto(): CommentDTO
    {
        return new CommentDTO($this->validated());
    }

}
