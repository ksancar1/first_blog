<?php

namespace App\Http\Requests\Backend\User;

use App\Enums\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class UpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {

        return [
            "id" => ["required", "uuid", Rule::exists((new User())->getTable(), "id")],
            "name" => ["required", "string"],
            "email" => [
                "required", "email",
                Rule::unique((new User())->getTable(), "email")->ignore($this->route("user"))
            ],
            "role" => ["required", new Enum(Role::class)],
            "password" => ["nullable", "string", "min:3", "max:32"],
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge(["id" => $this->route("user")]);
    }
}
