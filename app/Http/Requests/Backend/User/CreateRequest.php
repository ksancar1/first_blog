<?php

namespace App\Http\Requests\Backend\User;

use App\Enums\Role;
use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }


    public function rules(): array
    {
        return [
            "name"     => ["required", "string"],
            "email"    => ["required", "email", Rule::unique((new User())->getTable(), "email")],
            "role"     => ["required", new Enum(Role::class)],
            "password" => ["required", "string", "min:3", "max:32"],
        ];
    }
}
