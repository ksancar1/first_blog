<?php

namespace App\Http\Requests\Backend\Article;

use App\Models\Article;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RestoreRequest extends FormRequest
{

    public function authorize(): bool
    {
        return $this->user()->can("restore", Article::withTrashed()->find($this->article));
    }

    public function rules(): array
    {
        return [
            "id" => ["required", "uuid", Rule::exists((new Article())->getTable(), "id")]
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge(["id" => $this->route("article")]);
    }
}
