<?php

namespace App\Http\Requests\Backend\Article;

use App\Models\Article;
use App\Models\Image;
use App\Models\Rubric;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->can("update", Article::find($this->article));
    }

    public function rules(): array
    {
        return [
            "id"          => ["required", "uuid", Rule::exists((new Article())->getTable(), "id")],
            "name"        => [
                "required",
                "string",
                Rule::unique((new Article())->getTable(), "name")
                    ->ignore($this->route("article"))
            ],
            "rubric_id"   => ["required", "uuid", Rule::exists((new Rubric())->getTable(), "id")],
            "description" => ["string", "min:3", "max:200"],
            "content"     => ["required", "string", "min:100"],
            "cover"       => ["uuid", Rule::exists((new Image())->getTable(), "id")],
            "images"      => ["array"],
            "images.*"    => ["nullable", "image"]
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge(["id" => $this->route("article")]);
    }
}
