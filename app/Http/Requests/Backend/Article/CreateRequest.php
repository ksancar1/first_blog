<?php

namespace App\Http\Requests\Backend\Article;

use App\Models\Article;
use App\Models\Rubric;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->can("create", Article::class);
    }

    public function rules(): array
    {
        return [
            "name"        => ["required", "string", Rule::unique((new Article())->getTable(), "name")],
            "rubric_id"   => ["required", "uuid", Rule::exists((new Rubric())->getTable(), "id")],
            "description" => ["string", "min:3", "max:200"],
            "content"     => ["required", "string", "min:100"],
            "images"      => ["array"],
            "images.*"    => ["image"]
        ];
    }
}
