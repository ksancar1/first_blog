<?php

namespace App\Http\Requests\Backend\Rubric;

use App\Models\Rubric;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->can("create", Rubric::class);
    }

    public function rules(): array
    {
        return [
            "name" => ["required", "string", Rule::unique((new Rubric())->getTable(), "name")]
        ];
    }
}
