<?php

namespace App\Http\Requests\Backend\Rubric;

use App\Models\Rubric;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class DeleteRequest extends FormRequest
{

    public function authorize(): bool
    {
        return $this->user()->can("delete", Rubric::find($this->rubric));
    }


    public function rules(): array
    {
        return [
            "id" => ["required", "uuid", Rule::exists((new Rubric())->getTable(), "id")]
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge(["id" => $this->route("rubric")]);
    }
}
