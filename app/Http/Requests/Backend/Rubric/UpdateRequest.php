<?php

namespace App\Http\Requests\Backend\Rubric;

use App\Models\Rubric;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return $this->user()->can("update", Rubric::find($this->rubric));
    }

    public function rules(): array
    {
        return [
            "id"   => ["required", "uuid", Rule::exists((new Rubric())->getTable(), "id")],
            "name" => [
                "required",
                "string",
                Rule::unique((new Rubric())->getTable(), "name")
                    ->ignore($this->route("rubric"))
            ]
        ];
    }

    protected function prepareForValidation(): void
    {
        $this->merge(["id" => $this->route("rubric")]);
    }
}
