<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SearchRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            "query" => ["required", "string", "min:3", "max:100"],
            "rubric_id" => ["bail", "uuid", "exists:rubrics,id"]
        ];
    }
}
