<?php

namespace App\Enums;

enum Role: int
{
    case GUEST = 0;
    case SUBSCRIBER = 1;
    case VIP = 3;
    case ADMIN = 7;
    case KUBIK = 2;
    case EDITOR = 5;

    public function title(): string
    {
        return match ($this) {
            self::GUEST => "Гість",
            self::SUBSCRIBER => "Підписник",
            self::VIP => "Віп",
            self::ADMIN => "Адмін",
            self::KUBIK => "Дупа",
            self::EDITOR => "Редактор",
        };
    }

    public function color(): string
    {
        return match ($this) {
            self::GUEST => "bg-secondary",
            self::SUBSCRIBER => "bg-info",
            self::VIP => "bg-warning",
            self::ADMIN => "bg-danger",
            self::KUBIK => "bg-success",
            self::EDITOR => "bg-primary",
        };
    }

}
