<?php

namespace App\Providers;

use App\Contracts\CommentServiceContract;
use App\Contracts\RubricServiceContract;
use App\Services\CommentService;
use App\Services\RubricService;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(RubricServiceContract::class, fn() => new RubricService());
        $this->app->bind(CommentServiceContract::class, fn() => new CommentService());
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        Paginator::useBootstrapFour();
    }
}
