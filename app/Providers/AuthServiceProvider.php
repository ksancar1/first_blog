<?php

namespace App\Providers;


use App\Enums\Role;
use App\Models\Article;
use App\Models\Rubric;
use App\Models\User;
use App\Policies\ArticlePolicy;
use App\Policies\RubricPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        Article::class => ArticlePolicy::class,
        Rubric::class => RubricPolicy::class
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {
        Gate::define("see-rubrics-users", function (User $user) {
            return $user->role === Role::ADMIN;
        });
        Gate::define("article-add", function (User $user) {
            return $user->role !== Role::KUBIK;
        });
        Gate::define("article-edit", function (User $user) {
            return $user->role !== Role::KUBIK;
        });
        Gate::define("article-delete", function (User $user) {
            return $user->role !== Role::KUBIK;
        });
        Gate::define("rubric-add", function (User $user) {
            return $user->role !== Role::KUBIK;
        });
        Gate::define("rubric-edit", function (User $user) {
            return $user->role !== Role::KUBIK;
        });
        Gate::define("rubric-delete", function (User $user) {
            return $user->role !== Role::KUBIK;
        });
        Gate::define("see-author", function (User $user) {
            return $user->role !== Role::EDITOR;
        });
        Gate::define("can-restore", function (User $user) {
            return $user->role === Role::KUBIK;
        });

    }
}
