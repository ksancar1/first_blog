<?php

namespace Tests;

use App\Models\User;
use Database\Seeders\UserSeed;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    public function setUp(): void
    {
        parent::setUp();

        Artisan::call("migrate:fresh");
        $this->seed(UserSeed::class);
    }

    public function admin(): User
    {
        return User::find("00000000-0000-0000-0000-000000000001");
    }

    public function editor(): User
    {
        return User::find("00000000-0000-0000-0000-000000000006");
    }

    public function kubik(): User
    {
        return User::find("00000000-0000-0000-0000-000000000007");
    }
}
