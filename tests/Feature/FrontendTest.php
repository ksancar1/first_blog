<?php

namespace Tests\Feature;

use App\Models\Article;
use App\Models\Comment;
use App\Models\Rubric;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class FrontendTest extends TestCase
{

    use WithFaker;

    /**
     * @test
     */
    public function empty_frontpage()
    {
        $this
            ->get(route("frontpage"))
            ->assertOk()
            ->assertSee("Sirogas frontpage blog")
            ->assertSee("No rubrics found");
    }

    /**
     * @test
     */
    public function not_exists()
    {
        $this
            ->get(route("frontpage") . "/abc")
            ->assertNotFound();
    }

    /**
     * @test
     */
    public function frontpage()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->has(Article::factory())->create();
        $rubric->load("articles");

        $this
            ->get(route("frontpage"))
            ->assertOk()
            ->assertSee($rubric->name)
            ->assertSee($rubric->articles->first()->name);
    }

    /**
     * @test
     */
    public function rubric()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->has(Article::factory())->create();
        $rubric->load("articles");

        $this
            ->get(route("rubric", $rubric))
            ->assertOk()
            ->assertSee($rubric->name)
            ->assertSee($rubric->articles->first()->name);
    }

    /**
     * @test
     */
    public function article()
    {
        /** @var Article $article */
        $article = Article::factory()->has(Comment::factory())->create();
        $article->load("comments");

        $this->get(route("article", $article))
            ->assertOk()
            ->assertSee($article->name)
            ->assertSee($article->content)
            ->assertSee($article->comments->first()->name);
    }

    /**
     * @test
     */
    public function postComment_success()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        /** @var Article $article */
        $article = Article::factory()->create(["rubric_id" => $rubric->id]);

        $request = [
            "name"        => $this->faker->name,
            "email"       => $this->faker->email,
            "description" => $this->faker->text,
            "article_id"  => $article->id
        ];

        $this
            ->post(route("comment.store"), $request)
            ->assertRedirect(route("article", $article))
            ->assertSessionHas("success", "Comment has been created successfully");

        $this
            ->assertDatabaseHas((new Comment())->getTable(), $request);
    }

    /**
     * @test
     */
    public function postComment_fail_name()
    {
        /** @var Rubric $rubric */
        $rubric  = Rubric::factory()->create();
        /** @var Article $article */
        $article = Article::factory()->create(["rubric_id" => $rubric->id]);

        $request = [
            "email"       => $this->faker->email,
            "description" => $this->faker->text,
            "article_id"  => $article->id
        ];

        $this
            ->post(route("comment.store"), $request)
            ->assertInvalid("name");
        $request["name"] = "a";
        $this
            ->post(route("comment.store"), $request)
            ->assertInvalid("name");
        $request["name"] = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
        $this
            ->post(route("comment.store"), $request)
            ->assertInvalid("name");

        $this
            ->assertDatabaseMissing((new Comment())->getTable(), $request);
    }

    /**
     * @test
     */
    public function search_success()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        /** @var Article $article */
        $article = Article::factory()->create([
            "name" => "Test article",
            "rubric_id" => $rubric->id,
        ]);

        $request = [
            "query" => "Test",
            "rubric_id" => $rubric->id
        ];

        $this->post(route("search"), $request)
            ->assertOk()
            ->assertViewIs("frontend.search.index")
            ->assertSee($article->name);
    }

    /**
     * @test
     */
    public function search_fail()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        /** @var Article $article */
        $article = Article::factory()->create([
            "name" => "Test article",
            "rubric_id" => $rubric->id,
        ]);

        $request = [
            "query" => $this->faker->word(),
            "rubric_id" => $rubric->id
        ];

        $this->post(route("search"), ["query" => "aa"])
            ->assertSessionHasErrors("query");

        $this->get(route("search"), $request)
            ->assertSessionHasErrors("query");
    }

}
