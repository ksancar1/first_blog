<?php

namespace Tests\Feature\Backend;

use App\Models\Article;
use App\Models\Rubric;
use App\Models\User;
use Tests\TestCase;

class DashboardTest extends TestCase
{
    /**
     * @test
     */
    public function index()
    {
        $user = User::all();
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->has(Article::factory())->create();
        $rubric->load("articles");

        $this
            ->actingAs($this->admin())
            ->get(route("admin.dashboard"))
            ->assertOk()
            ->assertSee($user->first()->name)
            ->assertSee($rubric->articles->count());
    }
}
