<?php

namespace Tests\Feature\Backend;

use App\Models\Article;
use App\Models\Image;
use App\Models\Rubric;
use App\Models\User;
use App\Policies\ArticlePolicy;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    /**
     * @test
     */
    public function empty_index()
    {
        $this
            ->actingAs($this->admin())
            ->get(route("admin.article.index"))
            ->assertOk()
            ->assertSee("Articles")
            ->assertSee("Nothing found for your request");
    }

    /**
     * @test
     */
    public function index()
    {
        /** @var Collection $articles */
        $articles = Article::factory()->count(3)->create();

        $response = $this
            ->actingAs($this->admin())
            ->get(route("admin.article.index"))
            ->assertOk();

        $articles->each(fn(Article $article) => $response->assertSee($article->name));
    }

    /**
     * @test
     */
    public function create()
    {
        $this
            ->actingAs($this->admin())
            ->get(route("admin.article.create"))
            ->assertOk();
    }

    /**
     * @test
     */
    public function store()
    {
        Storage::fake("images");
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        /** @var Article $article */
        $article = Article::factory()->make(["rubric_id" => $rubric->id]);

        $request = [
            ...$article->toArray(),
            "images" => [
                UploadedFile::fake()->image("avatar.png"),
                UploadedFile::fake()->image("something.png"),
            ]
        ];

        $this
            ->actingAs($this->admin())
            ->post(route("admin.article.store"), $request, ["Content-type" => "multipart/form-data"])
            ->assertRedirect(route("admin.article.index"))
            ->assertSessionHas("success", "Article has been created successfully");

        $this->assertDatabaseCount($article->getTable(), 1);
        $this->assertDatabaseCount((new Image())->getTable(), 2);
        /** @var Article $article */
        $article = Article::query()->with("images")->first();
        $article->images->each(fn(Image $image) => Storage::disk("images")->assertExists($article->id . "/" . $image->filename));
    }

    /**
     * @test
     */
    public function edit()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        $article = Article::factory()->create(["rubric_id" => $rubric->id]);

        $this
            ->actingAs($this->admin())
            ->get(route("admin.article.edit", ["article" => $article]))
            ->assertOk();
    }

    /**
     * @test
     */
    public function update()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        /** @var Rubric $newRubric */
        $newRubric = Rubric::factory()->create();
        /** @var Article $article */
        $article = Article::factory()->has(Image::factory()->count(2))->create(["rubric_id" => $rubric->id]);
        $article->load("images");
        $coverId = $article->images->first()->id;
        $request = [
            "name"        => $article->name . "1",
            "description" => $article->description . "1",
            "content"     => $article->content . "1",
            "rubric_id"   => $newRubric->id,
            "cover"       => $coverId
        ];

        $this
            ->actingAs($this->admin())
            ->put(route("admin.article.update", $article), $request)
            ->assertRedirect(route("admin.article.edit", $article))
            ->assertSessionHas("success", "Article has been updated successfully");

        $this->assertDatabaseHas($article->getTable(), collect($request)->except("cover")->toArray());
        $this->assertDatabaseHas((new Image())->getTable(), ["id" => $coverId, "is_cover" => true]);
    }

    /**
     * @test
     */
    public function delete_article()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        $article = Article::factory()->create(["rubric_id" => $rubric->id]);

        $this
            ->actingAs($this->admin())
            ->delete(route("admin.article.destroy", ["article" => $article->id]))
            ->assertRedirect(route("admin.article.index"))
            ->assertSessionHas("success", "Article has been deleted successfully");

        $this->assertSoftDeleted("articles", ["id" => $article->id]);

    }

    /**
     * @test
     */
    public function restore_article()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        $article = Article::factory()->create(["rubric_id" => $rubric->id]);
        $article->delete();

        $this
            ->actingAs($this->admin())
            ->put(route("admin.article.restore", ["article" => $article->id]))
            ->assertRedirect(route("admin.article.index"))
            ->assertSessionHas("success", "Article has been restored successfully");

        $this->assertDatabaseHas("articles", ["id" => $article->id]);
    }

    /**
     * @test
     */
    public function see_deleted_articles()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        $article = Article::factory()->create(["rubric_id" => $rubric->id]);
        $article->delete();

        $this
            ->actingAs($this->kubik())
            ->get(route("admin.article.index"))
            ->assertOk()
            ->assertSee($article->name);
    }

    /**
     * @test
     */
    public function manage_only_created_articles()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        $article = Article::factory()->create(["rubric_id" => $rubric->id, "user_id" => $this->editor()->id]);
        $randomArticle = Article::factory()->create(["rubric_id" => $rubric->id, "user_id" => $this->admin()->id]);

        $this
            ->actingAs($this->editor())
            ->get(route("admin.article.index"))
            ->assertOk()
            ->assertSee($article->name)
            ->assertDontSee($randomArticle->name);

        $this
            ->actingAs($this->editor())
            ->put(route("admin.article.update", $randomArticle->id))
            ->assertForbidden();

        $this
            ->actingAs($this->editor())
            ->delete(route("admin.article.destroy", $randomArticle->id))
            ->assertForbidden();
    }

    /**
     * @test
     */
    public function testView()
    {
        $user = new User();
        $user->id = 1;

        $article = new Article();
        $article->user_id = 1;

        $policy = new ArticlePolicy();

        $this->assertTrue($policy->view($user, $article));
    }
}
