<?php

namespace Tests\Feature\Backend;

use App\Enums\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{

    use WithFaker;

    /**
     * @test
     */
    public function index()
    {
        $users = User::all();

        $response = $this->actingAs($this->admin())
            ->get(route("admin.user.index"))
            ->assertOk();

        $users->each(fn(User $user) => $response->assertSee($user->name));

    }

    /**
     * @test
     */
    public function create()
    {
        $this
            ->actingAs($this->admin())
            ->get(route("admin.user.create"))
            ->assertOk();
    }

    /**
     * @test
     */
    public function store()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $request = [
            "name" => $this->faker->name,
            "email" => $this->faker->email,
            "role" => Role::GUEST->value,
            "password" => $this->faker->password,
        ];

        $this
            ->actingAs($this->admin())
            ->post(route("admin.user.store"), $request)
            ->assertRedirect(route("admin.user.index"))
            ->assertSessionHas("success", "User has been created successfully");

        $this->assertDatabaseHas($user->getTable(), collect($request)->except("password")->toArray());
    }

    /**
     * @test
     */
    public function edit()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $this
            ->actingAs($this->admin())
            ->get(route("admin.user.edit", $user))
            ->assertOk();
    }

    /**
     * @test
     */
    public function update()
    {
        /** @var User $user */
        $user = User::factory()->create(["role" => Role::VIP]);


        $request = [
            "name" => $user->name . "1",
            "email" => $user->email . "1",
            "role" => Role::SUBSCRIBER->value,
            "password" => $this->faker->password . "1",
        ];

        $this
            ->actingAs($this->admin())
            ->put(route("admin.user.update", $user), $request)
            ->assertRedirect(route("admin.user.index"))
            ->assertSessionHas("success", "User has been updated successfully");

        $this->assertDatabaseHas($user->getTable(), collect($request)->except("password")->toArray());
    }

    /**
     * @test
     */
    public function delete_user()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $this
            ->actingAs($this->admin())
            ->delete(route("admin.user.destroy", $user))
            ->assertRedirect(route("admin.user.index"))
            ->assertSessionHas("success", "User has been deleted successfully");

        $this->assertDatabaseMissing("users", ["id" => $user->id]);

    }
}
