<?php

namespace Tests\Feature\Backend;

use App\Models\Rubric;
use App\Models\User;
use App\Policies\RubricPolicy;
use Illuminate\Support\Collection;
use Tests\TestCase;

class RubricTest extends TestCase
{
    /**
     * @test
     */
    public function index()
    {
        /** @var Collection $rubrics */
        $rubrics = Rubric::factory()->count(3)->create();

        $response = $this
            ->actingAs($this->admin())
            ->get(route("admin.rubric.index"))
            ->assertOk();

        $rubrics->each(fn(Rubric $rubric) => $response->assertSee($rubric->name));
    }

    /**
     * @test
     */
    public function create()
    {
        $this
            ->actingAs($this->admin())
            ->get(route("admin.rubric.create"))
            ->assertOk();
    }

    /**
     * @test
     */
    public function store_success()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->make();

        $this
            ->actingAs($this->admin())
            ->post(route("admin.rubric.store"), $rubric->only("name"))
            ->assertRedirect(route("admin.rubric.index"))
            ->assertSessionHas("success", "Rubric has been created successfully");

        $this->assertDatabaseHas($rubric->getTable(), $rubric->only("name"));
    }

    /**
     * @test
     */
    public function store_failed()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();

        $this
            ->actingAs($this->admin())
            ->post(route("admin.rubric.store"), $rubric->only("name"))
            ->assertInvalid("name");
    }

    /**
     * @test
     */
    public function index_as_editor()
    {
        $this
            ->actingAs($this->editor())
            ->get(route("admin.rubric.index"))
            ->assertRedirectToRoute("admin.dashboard")
            ->assertSessionHasErrors("message", "Ви не маєте доступу до цієї сторінки.");
    }

    /**
     * @test
     */
    public function edit()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();

        $this
            ->actingAs($this->admin())
            ->get(route("admin.rubric.edit", $rubric))
            ->assertOk();
    }

    /**
     * @test
     */
    public function update()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();

        $request = [
            "name" => $rubric->name . "a"
        ];

        $this
            ->actingAs($this->admin())
            ->put(route("admin.rubric.update", $rubric), $request)
            ->assertRedirect(route("admin.rubric.edit", $rubric))
            ->assertSessionHas("success", "Rubric has been updated successfully");

        $this->assertDatabaseHas($rubric->getTable(), collect($request)->toArray());
    }

    /**
     * @test
     */
    public function delete_rubric()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();

        $this
            ->actingAs($this->admin())
            ->delete(route("admin.rubric.destroy", ["rubric" => $rubric->id]))
            ->assertRedirect(route("admin.rubric.index"))
            ->assertSessionHas("success", "Rubric has been deleted successfully");

        $this->assertSoftDeleted("rubrics", ["id" => $rubric->id]);

    }

    /**
     * @test
     */
    public function restore_rubric()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        $rubric->delete();

        $this
            ->actingAs($this->admin())
            ->put(route("admin.rubric.restore", $rubric))
            ->assertRedirect(route("admin.rubric.index"))
            ->assertSessionHas("success", "Rubric has been restored successfully");

        $this->assertDatabaseHas("rubrics", ["id" => $rubric->id]);
    }

    /**
     * @test
     */
    public function see_deleted_rubrics()
    {
        /** @var Rubric $rubric */
        $rubric = Rubric::factory()->create();
        $rubric->delete();

        $this
            ->actingAs($this->kubik())
            ->get(route("admin.rubric.index"))
            ->assertStatus(200)
            ->assertSee($rubric->name);
    }

    /**
     * @test
     */
    public function testView()
    {
        $user = new User();
        $user->id = 1;

        $rubric = new Rubric();
        $rubric->user_id = 1;

        $policy = new RubricPolicy();

        $this->assertTrue($policy->view($user, $rubric));
    }
}
