<?php

namespace Tests\Feature\Backend;

use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;


class LoginTest extends TestCase
{
    use WithFaker;

    /**
     * @test
     */
    public function index()
    {
        $this
            ->get(route("admin.login"))
            ->assertOk();
    }

    /**
     * @test
     */
    public function valid_email()
    {
        $user = User::create([
            "name" => "Test User",
            "email" => "test@example.com",
            "password" => Hash::make("password"),
        ]);

        $request = [
            "email" => $user->email,
            "password" => $user->password,
        ];

        $this
            ->post(route("admin.login"), $request)
            ->assertSessionHasNoErrors();

        $request["email"] = "aaaaaaaaaaaa";

        $this
            ->post(route("admin.login"), $request)
            ->assertSessionHasErrors("email");
    }

    /**
     * @test
     */
    public function valid_password()
    {
        $user = User::create([
            "name" => "Test User",
            "email" => "test@example.com",
            "password" => Hash::make("password"),
        ]);

        $request = [
            "email" => $user->email,
            "password" => $user->password];

        $this
            ->post(route("admin.login"), $request)
            ->assertSessionHasNoErrors();

        $request["password"] = "13245";

        $this
            ->post(route("admin.login"), $request)
            ->assertSessionHasErrors("password");
    }


    /**
     * @test
     */
    public function valid_login()
    {
        $user = User::create([
            "name" => "Test User",
            "email" => "test@example.com",
            "password" => Hash::make("password"),
        ]);

        $response = $this->post(route("admin.login"), [
            "email" => "test@example.com",
            "password" => "password",
        ]);

        $response->assertRedirect(route("admin.dashboard"));
        $this->assertAuthenticatedAs($user);
    }

    /**
     * @test
     */
    public function invalid_email_login()
    {
        $user = User::create([
            "name" => "Test User",
            "email" => "test@example.com",
            "password" => Hash::make("password"),
        ]);

        $request = [
            "email" => "invalid@example.com",
            "password" => "password",
        ];

        $this->post(route("admin.login", $user), $request)
            ->assertRedirect(route("admin.form"))
            ->assertSessionHas("error", "Invalid credentials");
        $this->assertFalse($this->isAuthenticated());
    }

    /**
     * @test
     */
    public function invalid_password_login()
    {
        $user = User::create([
            "name" => "Test User",
            "email" => "test@example.com",
            "password" => Hash::make("password"),
        ]);

        $request = [
            "email" => "test@example.com",
            "password" => "invalidpassword",
        ];

        $this->post(route("admin.login", $user), $request)
            ->assertRedirect(route("admin.form"))
            ->assertSessionHas("error", "Invalid credentials");
        $this->assertFalse($this->isAuthenticated());
    }

    /**
     * @test
     */
    public function logout()
    {
        $this->actingAs($this->admin());

        $response = $this->get(route("admin.logout"));

        $response->assertRedirect(route("admin.form"));
        $this->assertFalse($this->isAuthenticated());
    }
}
