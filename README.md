# Blog

## Requirements

- [Docker]()
- [Docker Compose]()

## Installation

1. Clone repo
2. `docker-compose up -d`
3. `docker-compose exec php bash`
4. `cp .env.example .env`
5. `composer install`
6. `php artisan key:generate`
7. Open in browser [localhost](http://localhost)
