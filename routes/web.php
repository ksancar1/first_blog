<?php

use App\Http\Controllers\Backend\ArticleController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\LoginController;
use App\Http\Controllers\Backend\RubricController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Frontend\FrontendController;
use Illuminate\Support\Facades\Route;

Route::get("/", [FrontendController::class, "index"])->name("frontpage");
Route::get("/rubric/{rubric}", [FrontendController::class, "rubric"])->name("rubric");
Route::get("/article/{article}", [FrontendController::class, "article"])->name("article");
Route::any("/search", [FrontendController::class, "search"])->name("search");
Route::post("/comment", [FrontendController::class, "storeComment"])->name("comment.store");

/**
 * admin
 */
Route::group(["prefix" => "admin", "as" => "admin."], function () {
    Route::get("", [LoginController::class, "form"])->name("form");
    Route::post("", [LoginController::class, "login"])->name("login");
    Route::group(["middleware" => ["admin"]], function () {
        Route::get("dashboard", DashboardController::class)->name("dashboard");
        Route::get("logout", [LoginController::class, "logout"])->name("logout");
        Route::resource("article", ArticleController::class)->except("show");
        Route::put("article/{article}/restore", [ArticleController::class, "restore"])->name("article.restore");
        Route::group(["middleware" => ["editor"]], function () {
            Route::resource("rubric", RubricController::class)->except("show");
            Route::put("rubric/{rubric}/restore", [RubricController::class, "restore"])->name("rubric.restore");
            Route::group(["middleware" => ["kubik"]], function () {
                Route::resource("user", UserController::class)->except("show");
            });
        });
    });
});
