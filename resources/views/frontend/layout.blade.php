<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield("title")</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <link rel="stylesheet" href="{{ asset("css/bootstrap.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/bootstrap-theme.min.css") }}">
    <link rel="stylesheet" href="{{ asset("css/fontAwesome.css") }}">
    <link rel="stylesheet" href="{{ asset("css/light-box.css") }}">
    <link rel="stylesheet" href="{{ asset("css/templatemo-style.css") }}">

    <link href="https://fonts.googleapis.com/css?family=Kanit:100,200,300,400,500,600,700,800,900" rel="stylesheet">

</head>

<body>

<nav id="top_menu">
    <div class="logo">
        <a href="{{ route("frontpage") }}">Siroga's blog</a>
    </div>
</nav>

<div class="page-heading">
    <div class="container">
        <div class="heading-content">
            <h1>@yield("title")</h1>
        </div>
    </div>
</div>


<div class="blog-entries">
    <div class="container">
        <div class="col-md-9">
            <div class="blog-posts">
                <div class="row">
                    @yield("content")
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="side-bar">
                <div class="search">
                    <fieldset>
                        @yield("form")
                    </fieldset>
                </div>
                @yield("sidebar")
            </div>
        </div>
    </div>
</div>


<footer>
    <div class="container-fluid">
        <div class="col-md-12">
            <p>Copyright &copy; 2018 Company Name | Designed by TemplateMo</p>
        </div>
    </div>
</footer>

</body>
</html>
