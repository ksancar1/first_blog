@extends("frontend.layout")

@section("title",$rubric->name)

@section("content")
    @foreach($articles as $article)
        @include("frontend.article",["article"=>$article,"rubric"=>false])
    @endforeach

    <div class="col-md-12">
        {{ $articles->links() }}
    </div>

@endsection

@section("form")
    <form action="{{route("search")}}" method="post">
        @csrf
        <input type="hidden" name="rubric_id" value="{{ $rubric->id }}">
        <input name="query" value="{{ request("query") }}" type="text" class="form-control" id="search"
               placeholder="Search..."
               required="" autocomplete="off"/>
        <button type="submit">Search</button>
    </form>
@endsection
