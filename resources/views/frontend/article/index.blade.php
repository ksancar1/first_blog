@extends("frontend.layout")

@section("title", $article->name)



@section("content")
    <div class="col-md-12">
        <div class="blog-post">
            <div class="text-content">
                    <span>
                        <a href="#">{{ $article->created_at->format("j M Y") }}</a>
                    </span>
                <h4>{{ $article->description }}</h4>
            </div>
        </div>
    </div>
    @if($article->hasImages())
        <div class="col-md-12">
            <div class="row">
                @foreach($article->images as $image)
                    <div class="col-2">
                        <img src="{{ $image->getUrl() }}" alt="" class="img-thumbnail"/>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    <div class="col-md-12">
        <div class="blog-post">
            <div class="text-content">
                <p>{{ $article->content }}</p>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="blog-post">
            <div class="text-content">
                <form action="{{ route("comment.store") }}" id="comment-form" class="text-content"
                      method="post">
                    @csrf
                    <input type="hidden" name="article_id" value="{{ $article->id }}">
                    <div class="input-group">
                        <label id="name-label" for="comment-name">Name</label>
                        <input type="text" name="name" id="comment-name" class="name">
                    </div>
                    <div class="input-group">
                        <label id="email-label" for="comment-email">Email</label>
                        <input type="text" name="email" id="comment-email" class="email">
                    </div>
                    <div class="input-group">
                        <label id="label" for="comment-description">Comment below, please</label>
                        <textarea id="comment-description" rows="5" name="description" class="input-area"></textarea>
                    </div>
                    <div class="input-group">
                        <button type="submit" id="submit" class="submit-button btn-success">Add comment</button>
                    </div>
                </form>
                @if (session("success"))
                    <div class="alert alert-success">
                        {{ session("success") }}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="blog-post">
        <h3>Comments</h3>
    </div>
    @foreach($article->comments as $comment)
        <div class="col-md-12">
            <div class="blog-post">
                <div class="text-content">
                     <span>
                         <a href="#">{{ $comment->name }}</a> /
                         <a href="#">{{ $comment->email }}</a> /
                         <a href="#">{{ $comment->created_at->format("d-m-Y H:i:s") }}</a>
                    </span>
                    <p>{{ $comment->description }}</p>
                </div>
            </div>
        </div>
    @endforeach
@endsection
