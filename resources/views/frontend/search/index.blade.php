@extends("frontend.layout")

@section("title", count($articles) . " search results found")

@section("content")
    @foreach($articles as $article)
        @include("frontend.article",["article"=>$article,"rubric"=>true])
    @endforeach

    <div class="col-md-12">
        {{ $articles->links() }}
    </div>

@endsection


@section("sidebar")
    <div class="categories">
        <div class="sidebar-heding">
            <h2>Rubrics</h2>
        </div>
        <ul>
            @forelse($rubrics as $rubric)
                <li>
                    <a href="{{ route("search") }}?query={{ request("query") }}&rubric_id={{ $rubric->id }}">> {{ $rubric->name }}
                        ({{ $rubric->articles_count }})</a>
                </li>
            @empty
                <li>No rubrics found</li>
            @endforelse
        </ul>
    </div>
@endsection

@section("form")
    <form action="{{route("search")}}" method="post">
        @csrf
        <input type="hidden" name="rubric_id" value="{{ $rubric->id }}">
        <input name="query" value="{{ request("query") }}" type="text" class="form-control" id="search"
               placeholder="Search..."
               required="" autocomplete="off"/>
        <button type="submit">Search</button>
    </form>
@endsection
