<div class="col-md-12">
    <div class="blog-post">
        @include("frontend.cover",$article)
        <div class="text-content">
                    <span>
{{--                        <a href="#">Admin</a> / --}}
                        <a href="#">{{ $article->created_at->format("j M Y") }}</a>
                        @if($rubric)
                            / <a href="{{ route("rubric",$article->rubric) }}">{{ $article->rubric->name }}</a>
                        @endif
                    </span>
            <h2>{{ $article->name }}</h2>
            <p>{{ $article->description }}</p>
            <div class="simple-btn">
                <a href="{{route("article", $article)}}">continue reading</a>
            </div>
        </div>
    </div>
</div>
