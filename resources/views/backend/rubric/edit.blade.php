@extends("backend.layout")

@section("title","Edit rubric ".$rubric->name)

@section("content")
    <form action="{{ route("admin.rubric.update", $rubric) }}" method="post">
        @method("patch")
        @csrf
        <div class="form-group">
            <label for="exampleFormControlInput1">Name</label>
            <input type="text" name="name" class="form-control" value="{{ $rubric->name }}" placeholder="Enter name">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">
                Update
            </button>
        </div>
    </form>
@endsection
