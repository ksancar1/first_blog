@extends("backend.layout")


@section("title","Rubrics")


@section("content")
    <div class="row">
        <div class="col-10">
            <form class="form-inline" action="" method="get">
                <input type="text" name="q" value="{{ request("q") }}" class="form-control mb-2 mr-sm-2"
                       placeholder="Search request">
                <button type="submit" class="btn btn-primary mb-2">Search</button>
            </form>
        </div>
        @can("article-add")
        <div class="col-2 text-right">
            <a href="{{ route("admin.rubric.create") }}" class="btn btn-success">Create new</a>
        </div>
        @endcan
    </div>
    <table class="table table-striped table-sm table-hover table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th width="50px">Articles</th>
            <th width="140px">Created at</th>
            @can("can-restore")
                <th width="10%">Restore</th>
            @endcan
            @can("article-add")
                <th width="5%" colspan="2"></th>
            @endcan
        </tr>
        </thead>
        <tbody>
        @forelse($rubrics as $rubric)
            <tr>
                <td><a href="{{ $rubric->url() }}" target="_blank">{{ $rubric->name }}</a></td>
                <td>{{ $rubric->articles_count }}</td>
                <td>{{ $rubric->created_at }}</td>
                @can("can-restore")
                    <td width="50px">
                        <form action="{{ route("admin.rubric.restore", $rubric) }}" method="post" class="form-inline">
                            @method("put")
                            @csrf
                            <button type="submit" class="btn btn-sm btn-primary">Restore</button>
                        </form>
                    </td>
                @endcan
                @can("rubric-edit")
                    <td width="50px">
                        <a href="{{ route("admin.rubric.edit", $rubric) }}" class="btn btn-sm btn-primary">Edit</a>
                    </td>
                @endcan
                @can("rubric-delete")
                    <td width="50px">
                        <form action="{{ route("admin.rubric.destroy", $rubric) }}" method="post" class="form-inline">
                            @method("DELETE")
                            @csrf
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>
                    </td>
                @endcan
            </tr>
        @empty
            <tr>
                <td colspan="5">
                    <h3 class="text-center text-danger">
                        Nothing found for your request
                    </h3>
                </td>
            </tr>
        @endforelse

        </tbody>
    </table>
    <div class="pagination pagination-seperated ">
        {{ $rubrics->links() }}
    </div>
@endsection
