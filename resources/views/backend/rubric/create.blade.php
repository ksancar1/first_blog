@extends("backend.layout")

@section("title","Create new rubric")

@section("content")
    <form action="{{ route("admin.rubric.store") }}" method="post">
        @csrf
        <div class="form-group">
            <label for="exampleFormControlInput1">Name</label>
            <input type="text" name="name" class="form-control" id="exampleFormControlInput1" placeholder="Enter name">
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">
                Save
            </button>
        </div>
    </form>
@endsection
