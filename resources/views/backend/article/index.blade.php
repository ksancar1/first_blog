@extends("backend.layout")


@section("title","Articles")


@section("content")
    <div class="row">
        <div class="col-10">
            <form class="form-inline" action="" method="get">
                <input type="text" name="q" value="{{ request("q") }}" class="form-control mb-2 mr-sm-2"
                       placeholder="Search request">
                <button type="submit" class="btn btn-primary mb-2">Search</button>
            </form>
        </div>
        @can("article-add")
            <div class="col-2 text-right">
                <a href="{{ route("admin.article.create") }}" class="btn btn-success">Create new</a>
            </div>
        @endcan
    </div>
    <table class="table table-striped table-sm table-hover table-bordered">
        <thead>
        <tr>
            <th width="41%">Name</th>
            <th width="31%">Rubric</th>
            @can("see-author")
                <th width="10%">Author</th>
            @endcan
            <th width="3%">Comments</th>
            <th width="10%">Created at</th>
            @can("can-restore")
                <th width="10%">Restore</th>
            @endcan
            @can("article-add")
                <th width="5%" colspan="2"></th>
            @endcan
        </tr>
        </thead>
        <tbody>
        @forelse($articles as $article)
            <tr>
                <td><a href="{{ $article->url() }}" target="_blank">{{ $article->name }}</a></td>
                @if($article->rubric)
                    <td><a href="{{ route("rubric", $article->rubric_id) }}">{{ $article->rubric->name }}</a></td>
                @else
                    <td>Rubric deleted</td>
                @endif
                @can("see-author")
                    <td>{{ $article->author->name }}</td>
                @endcan
                <td>{{ $article->comments_count }}</td>
                <td>{{ $article->created_at }}</td>
                @can("can-restore")
                    <td width="50px">
                        <form action="{{ route("admin.article.restore", $article) }}" method="post" class="form-inline">
                            @method("put")
                            @csrf
                            <button type="submit" class="btn btn-sm btn-primary">Restore</button>
                        </form>
                    </td>
                @endcan
                @can("article-edit")
                    <td width="50px">
                        <a href="{{ route("admin.article.edit", $article) }}" class="btn btn-sm btn-primary">Edit</a>
                    </td>
                @endcan
                @can("article-delete")
                    <td width="50px">
                        <form action="{{ route("admin.article.destroy", $article) }}" method="post" class="form-inline">
                            @method("DELETE")
                            @csrf
                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                        </form>
                    </td>
                @endcan
            </tr>
        @empty
            <tr>
                <td colspan="5">
                    <h3 class="text-center text-danger">
                        Nothing found for your request
                    </h3>
                </td>
            </tr>
        @endforelse

        </tbody>
    </table>
    <div class="pagination pagination-seperated ">
        {{ $articles->links() }}
    </div>
@endsection
