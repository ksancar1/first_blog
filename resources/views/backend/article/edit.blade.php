@extends("backend.layout")

@section("title","Edit article ".$article->name)

@section("content")
    <form action="{{ route("admin.article.update", $article) }}" method="post">
        @method("put")
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" value="{{ $article->name }}" placeholder="Enter name">
        </div>

        <div class="form-group">
            <label for="rubric_id">Choose the rubric for the article</label>
            <select type="select" name="rubric_id" class="form-control" value="{{ $article->rubric_id }}">
                @foreach($rubrics as $rubric)
                    <option
                        value="{{ $rubric->id }}" {{ $article->rubric_id === $rubric->id ? "selected" : "" }}>{{ $rubric->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <input name="description" class="form-control" value="{{ $article->description }}"
                   placeholder="Enter description">
        </div>
        <div class="form-group">
            <label for="images">Images</label>
            <input type="file" multiple name="images[]" accept="image/*" class="form-control" id="images">
        </div>
        <div class="form-group">
            <label for="images">Cover</label>
            <div class="row">
                @forelse($article->images as $image)
                    <div class="col-3">
                        <label>
                            <img src="{{ $image->getUrl() }}" alt=""
                                 class="img-thumbnail @if($image->is_cover) bg-primary @endif ">
                            <input type="radio" class="hidd" name="cover"
                                   value="{{ $image->id }}" @checked($image->is_cover) />
                        </label>
                    </div>
                @empty
                    <div class="col-12 text-center text-danger">
                        Nothing uploaded
                    </div>
                @endforelse
            </div>
        </div>
        <div class="form-group">
            <label for="content">Content</label>
            <textarea rows="7" name="content" class="form-control" value="{{ $article->content }}"
                      placeholder="Enter content">{{ $article->content }}
            </textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">
                Update
            </button>
        </div>
    </form>
@endsection
