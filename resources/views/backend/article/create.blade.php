@extends("backend.layout")

@section("title","Create new article")

@section("content")
    <form action="{{ route("admin.article.store") }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter name"
                   value="{{ old("name") }}">
        </div>

        <div class="form-group">
            <label for="rubric_id">Choose the rubric for the article</label>
            <select type="select" name="rubric_id" class="form-control" id="rubric_id">
                @foreach($rubrics as $rubric)
                    <option value="{{ $rubric->id }}">{{ $rubric->name }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="description">Description</label>
            <input type="text" name="description" class="form-control" id="description"
                   placeholder="Enter description" value="{{ old("description") }}">
        </div>
        <div class="form-group">
            <label for="images">Images</label>
            <input type="file" multiple name="images[]" accept="image/*" class="form-control" id="images">
        </div>
        <div class="form-group">
            <label for="content">Content</label>
            <textarea name="content" rows="7" class="form-control" id="content"
                      placeholder="Enter content" value="{{ old("content") }}"></textarea>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-success">
                Save
            </button>
        </div>
    </form>
@endsection
