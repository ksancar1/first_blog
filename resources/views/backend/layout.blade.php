<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>@yield("title","Admin panel")</title>
    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500"
          rel="stylesheet"/>
    <link href="https://cdn.materialdesignicons.com/4.4.95/css/materialdesignicons.min.css" rel="stylesheet"/>

    <link id="sleek-css" rel="stylesheet" href="{{ asset("assets/css/sleek.css") }}"/>
</head>
<body class="header-fixed sidebar-fixed sidebar-dark header-light" id="body">
<!-- ====================================
——— WRAPPER
===================================== -->
<div class="wrapper">
    <!-- ====================================
      ——— LEFT SIDEBAR WITH OUT FOOTER
    ===================================== -->
    <aside class="left-sidebar bg-sidebar">
        <div id="sidebar" class="sidebar sidebar-with-footer">
            <!-- Aplication Brand -->
            <div class="app-brand">
                <a href="{{ route("frontpage") }}" title="Sleek Dashboard">
                    <svg
                        class="brand-icon"
                        xmlns="http://www.w3.org/2000/svg"
                        preserveAspectRatio="xMidYMid"
                        width="30"
                        height="33"
                        viewBox="0 0 30 33">
                        <g fill="none" fill-rule="evenodd">
                            <path class="logo-fill-blue" fill="#7DBCFF" d="M0 4v25l8 4V0zM22 4v25l8 4V0z"/>
                            <path class="logo-fill-white" fill="#FFF" d="M11 4v25l8 4V0z"/>
                        </g>
                    </svg>
                    <span class="brand-name text-truncate">{{ env("APP_NAME") }}</span>
                </a>
            </div>
            <!-- begin sidebar scrollbar -->
            <div class="" data-simplebar style="height: 100%;">
                <!-- sidebar menu -->
                <ul class="nav sidebar-inner" id="sidebar-menu">
                    <li>
                        <a class="sidenav-item-link" href="{{ route("admin.dashboard") }}">
                            <i class="mdi mdi-view-dashboard-outline"></i>
                            <span class="nav-text">Dashboard</span>
                        </a>
                    </li>
                    @can("see-rubrics-users")
                        <li>
                            <a class="sidenav-item-link" href="{{ route("admin.rubric.index") }}">
                                <i class="mdi mdi-view-dashboard-outline"></i>
                                <span class="nav-text">Rubrics</span>
                            </a>
                        </li>
                        <li>
                            <a class="sidenav-item-link" href="{{ route("admin.user.index") }}">
                                <i class="mdi mdi-view-dashboard-outline"></i>
                                <span class="nav-text">Users</span>
                            </a>
                        </li>
                    @elsecan("can-restore")
                        <li>
                            <a class="sidenav-item-link" href="{{ route("admin.rubric.index") }}">
                                <i class="mdi mdi-view-dashboard-outline"></i>
                                <span class="nav-text">Rubrics</span>
                            </a>
                        </li>
                    @endcan
                    <li>
                        <a class="sidenav-item-link" href="{{ route("admin.article.index") }}">
                            <i class="mdi mdi-view-dashboard-outline"></i>
                            <span class="nav-text">Articles</span>
                        </a>
                    </li>

                    <li>
                        <a class="sidenav-item-link" href="{{ route("admin.logout") }}">
                            <i class="mdi mdi-view-dashboard-outline"></i>
                            <span class="nav-text">Logout</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </aside>
    <!-- ====================================
  ——— PAGE WRAPPER
  ===================================== -->
    <div class="page-wrapper">
        <!-- ====================================
        ——— CONTENT WRAPPER
        ===================================== -->
        <div class="content-wrapper">
            <div class="content">
                <div class="bg-white border rounded">
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-default">
                                <div class="card-header">
                                    <h2>@yield("title")</h2>
                                </div>

                                <div class="card-body">
                                    @if (session("success"))
                                        <div class="alert alert-success">
                                            {{ session("success") }}
                                        </div>
                                    @endif
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    @yield("content")
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- End Content -->
        </div> <!-- End Content Wrapper -->
    </div> <!-- End Page Wrapper -->
</div> <!-- End Wrapper -->
</body>
</html>
