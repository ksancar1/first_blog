@extends("backend.layout")

@section("title","Dashboard")

@section("content")
    <h1 style="margin-bottom: 30px">Hello, {{ $user->name }}</h1>
    @can("see-rubrics-users")
    <table class="table table-striped table-sm table-hover table-bordered">
        <thead>
        <tr>
            <th width="41%">Name</th>
            <th width="7%">Posted</th>
            <th width="7%">Deleted</th>
        </tr>
        </thead>
        <tbody>
        @forelse($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->articles_count }}</td>
                <td><span>{{ $user->articles_deleted_count }}</span></td>
            </tr>
        @empty
            <tr>
                <td colspan="5">
                    <h3 class="text-center text-danger">
                        Nothing found for your request
                    </h3>
                </td>
            </tr>
        @endforelse

        </tbody>
    </table>
    @endcan
@endsection
