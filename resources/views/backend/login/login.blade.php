<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <title>{{ env("APP_NAME") }}</title>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Poppins:400,500,600,700|Roboto:400,500"
          rel="stylesheet"/>
    <link id="sleek-css" rel="stylesheet" href="{{ asset("assets/css/sleek.css") }}"/>
</head>

<body class="" id="body">
<div class="container d-flex align-items-center justify-content-center vh-100">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-10">
            <div class="card">
                <div class="card-header bg-primary">
                    <div class="app-brand">
                        <svg class="brand-icon" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid"
                             width="30" height="33"
                             viewBox="0 0 30 33">
                            <g fill="none" fill-rule="evenodd">
                                <path class="logo-fill-blue" fill="#7DBCFF" d="M0 4v25l8 4V0zM22 4v25l8 4V0z"/>
                                <path class="logo-fill-white" fill="#FFF" d="M11 4v25l8 4V0z"/>
                            </g>
                        </svg>

                        <span class="brand-name">Admin panel</span>
                    </div>
                </div>

                <div class="card-body p-5">

                    <form action="{{ route("admin.login") }}" method="post">

                        @if (session("error"))
                            <div class="alert alert-danger">
                                {{ session("error") }}
                            </div>
                        @endif
                        @csrf
                        <div class="row">
                            <div class="form-group col-md-12 mb-4">
                                <input type="email" name="email" class="form-control input-lg" id="email"
                                       value="{{ old("email") }}"
                                       aria-describedby="emailHelp" placeholder="Username">
                            </div>

                            <div class="form-group col-md-12 ">
                                <input type="password" name="password" class="form-control input-lg" id="password"
                                       placeholder="Password">
                            </div>

                            <div class="col-md-12">

                                <button type="submit" class="btn btn-lg btn-primary btn-block mb-4">Login</button>

                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


</body>
</html>
