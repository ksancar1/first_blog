@extends("backend.layout")

@section("title","Create new user")

@section("content")
    <form action="{{ route("admin.user.store") }}" method="post">
        @csrf
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" name="name" class="form-control" id="name" placeholder="Enter name">
        </div>

        <div class="form-group">
            <label for="email">Email</label>
            <input type="text" name="email" class="form-control" id="email"
                   placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="role">Role</label>
            <select name="role" id="role" class="form-control">
                @foreach($roles as $role)
                    <option value="{{ $role->value }}">{{ $role->title() }}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="pass">Password</label>
            <input type="password" name="password" class="form-control" id="pass"
                   placeholder="Enter password">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">
                Save
            </button>
        </div>
    </form>
@endsection
