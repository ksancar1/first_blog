@extends("backend.layout")


@section("title","Users")


@section("content")
    <div class="row">
        <div class="col-10">
            <form class="form-inline" action="" method="get">
                <input type="text" name="q" value="{{ request("q") }}" class="form-control mb-2 mr-sm-2"
                       placeholder="Search request">
                <button type="submit" class="btn btn-primary mb-2">Search</button>
            </form>
        </div>
        <div class="col-2 text-right">
            <a href="{{ route("admin.user.create") }}" class="btn btn-success">Create new</a>
        </div>
    </div>
    <table class="table table-striped table-sm table-hover table-bordered">
        <thead>
        <tr>
            <th width="41%">Name</th>
            <th width="41%">Email</th>
            <th width="13%">Role</th>
            <th width="5%" colspan="2"></th>
        </tr>
        </thead>
        <tbody>
        @forelse($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <span class="badge rounded-pill {{ $user->role->color() }}">{{ $user->role->title() }}</span>
                </td>
                <td width="50px">
                    <a href="{{ route("admin.user.edit", $user) }}" class="btn btn-sm btn-primary">Edit</a>
                </td>
                <td width="50px">
                    <form action="{{ route("admin.user.destroy", $user) }}" method="post" class="form-inline">
                        @method("DELETE")
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="5">
                    <h3 class="text-center text-danger">
                        Nothing found for your request
                    </h3>
                </td>
            </tr>
        @endforelse

        </tbody>
    </table>
    <div class="pagination pagination-seperated ">
        {{ $users->links() }}
    </div>
@endsection
