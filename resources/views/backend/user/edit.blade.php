@extends("backend.layout")

@section("title","Update users' info")

@section("content")
    <form action="{{ route("admin.user.update", $user) }}" method="post">
        @method("put")
        @csrf
        <div class="form-group">
            <label for="exampleFormControlInput1">Name</label>
            <input type="text" name="name" class="form-control" value="{{ $user->name }}" placeholder="Enter name">
        </div>

        <div class="form-group">
            <label for="exampleFormControlInput1">Email</label>
            <input type="text" name="email" class="form-control" value="{{ $user->email }}" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="role">Role</label>
            <select name="role" id="role" class="form-control">
                @foreach($roles as $role)
                    <option value="{{ $role->value }}" @if($role === $user->role) selected @endif>
                        {{ $role->title() }}
                    </option>
                @endforeach
            </select>
        </div>

        <div class="form-group">
            <label for="exampleFormControlInput3">Password</label>
            <input type="text" name="password" class="form-control"
                   placeholder="Enter password">
        </div>

        <div class="form-group">
            <button type="submit" class="btn btn-success">
                Save
            </button>
        </div>
    </form>
@endsection
